<?php
namespace Oliverbode\ParallaxWidget\Model\Config\Source;

use Magento\Cms\Model\BlockFactory;
 
class Blocks implements \Magento\Framework\Option\ArrayInterface
{

    protected $blockFactory;

    public function __construct(
        BlockFactory $blockFactory
    ) {
        $this->_blockFactory = $blockFactory;
    }


    public function toOptionArray()
    {
    	$options = array();
    	$blockCollection = $this->_blockFactory->create()->getCollection()->addFieldToFilter('is_active', 1);
     	foreach($blockCollection as $block) {
     		$options[] = array('value' => $block->getBlockId(), 'label' => __($block->getIdentifier()));
		}
		return $options;
    }

}