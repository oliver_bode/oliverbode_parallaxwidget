<?php
namespace Oliverbode\ParallaxWidget\Block;

use Magento\Cms\Model\Template\FilterProvider;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\View\Element\Template\Context;

class Parallax extends \Magento\Framework\View\Element\Template
{

    protected $filterProvider;

    protected $blockFactory;


    public function __construct(
        Context $context,
        FilterProvider $filterProvider,
        BlockFactory $blockFactory,
        array $data = []
    ) {
        parent::__construct($context,$data);
        $this->_filterProvider = $filterProvider;
        $this->_blockFactory = $blockFactory;
    }

    private function xmlAttribute($object, $attribute)
    {
        if (isset($object[$attribute]))
            return (string) $object[$attribute];
    }

    public function getBlockId() {
        return $this->getBlocks();
    }

    public function getContent($blockId) {
        $storeId = $this->_storeManager->getStore()->getId();
        $block = $this->_blockFactory->create();
        $block->setStoreId($storeId)->load($blockId);           
        $content = $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->filter($block->getContent());
        return $content;
    }


    public function getStaticHtml($blockId)
    {   
        $content = $this->getContent($blockId);
        $dom = new \DOMDocument();
        $dom->loadHTML($content);
        $xpath = new \DOMXPath($dom);

        $html = '<div class="static">';
        foreach ($xpath->query('//div[@class="static"]/*') as $node)
        {
             $html .= $dom->saveXML($node);
        }
        return $html . '</div>';
    }


    public function getParallaxOptions($blockId)
    {   
        $otherOptions = (!empty($this->getOptions())) ? ',' . $this->getOptions() :'';
        $content = $this->getContent($blockId);
        $dom = new \DOMDocument();
        $dom->loadHTML($content);
        $xml = simplexml_import_dom($dom)->xpath("//img/@src");
        $src = $this->xmlAttribute($xml[0], 'src'); 
        return  'imageSrc: \'' . $src . '\'' . $otherOptions;
    }
}