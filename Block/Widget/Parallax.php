<?php
namespace Oliverbode\ParallaxWidget\Block\Widget;

class Parallax extends \Oliverbode\ParallaxWidget\Block\Parallax implements \Magento\Widget\Block\BlockInterface
{
    public function getTemplate()
    {
        if (is_null($this->_template)) {
            $this->_template = 'Oliverbode_ParallaxWidget::parallax.phtml';
        }
        return $this->_template;
    }
}
