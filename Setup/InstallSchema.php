<?php
namespace Oliverbode\ParallaxWidget\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Cms\Model\Block;
use Magento\Cms\Model\Page;
use Magento\Cms\Controller\Adminhtml\Page\PostDataProcessor;


class InstallSchema implements InstallSchemaInterface {

    protected $pageFactory;

    protected $dataProcessor;

    protected $blockFactory;


    public function __construct(
    	Page $pageFactory,
    	PostDataProcessor $dataProcessor,
    	Block $blockFactory 
    ) 
    {
        $this->_pageFactory = $pageFactory;
        $this->dataProcessor = $dataProcessor;
        $this->_blockFactory = $blockFactory;
    }

    private function _toArray($xml) {
        $array = json_decode(json_encode($xml), true);
        foreach (array_slice($array, 0) as $key => $value) {
            if (is_string($value)) $array[$key] = trim($value);
            elseif (is_array($value) && empty($value)) $array[$key] = NULL;
            elseif (is_array($value)) {
                $array[$key] = $this->_toArray($value);
            }
        }
        return $array;
    }

    private function getModuleDir() {
        $manual = BP . '/app/code/Oliverbode/ParallaxWidget/';
        $composer = BP . '/vendor/oliverbode/parallaxwidget/';
        if (file_exists($manual)) return $manual;
        else if (file_exists($composer)) return $composer;
        else die(__('Unable to find module directory'));
    }

    private function symlinkImages() {
    	$modulePath = $this->getModuleDir();
    	$images = array('parallax-image-1.jpg','parallax-image-2.jpg','parallax-image-3.jpg');
    	foreach ($images as $image) {
            $source = $modulePath . 'Setup/data/' . $image;
            $dest = $modulePath . 'view/frontend/web/images/' . $image;
            try {
                copy($source,$dest);
	        } catch (\Exception $exception) {
	            die($exception->getMessage());
	        }
	    }
    }

    private function importBlocks() {
        $blocksFile = $this->getModuleDir() . 'Setup/data/blocks.xml';
        $xml = simplexml_load_string(file_get_contents($blocksFile), null, LIBXML_NOCDATA);
        $cmsBlock = $this->_blockFactory;
        try {
        	$blockIds = array();
            foreach ($xml->blocks as $block) {
                $blockId = $cmsBlock->getCollection()
                    ->addFieldToFilter('identifier', $block->identifier)
                    ->getFirstItem()
                    ->getId();
                $dataArray = $this->_toArray($block);
                $data = array();
                foreach($dataArray as $key => $value) {
                    if ($key == 'store_id') {
                        for ($i = 0; $i < count($dataArray['store_id']['store']); $i++) {
                            $data[$key][$i] = $dataArray['store_id']['store'][$i];
                        }
                    }
                    else $data[$key] = $value;
                }
                $cmsBlock->setBlockId($blockId);
                $cmsBlock->setData($data);
                $cmsBlock->save();
                $blockIds[] = $cmsBlock->getBlockId();
            }
            return $blockIds;
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }

    private function importPage($blockIds) {
        $pagesFile = $this->getModuleDir() . 'Setup/data/pages.xml';
        $xml = simplexml_load_string(file_get_contents($pagesFile), null, LIBXML_NOCDATA);
        $cmsPage = $this->_pageFactory;
        try {
            foreach ($xml->pages as $page) {
                $pageId = $cmsPage->getCollection()
                    ->addFieldToFilter('identifier', $page->identifier)
                    ->getFirstItem()
                    ->getId();
                $dataArray = $this->_toArray($page);
                foreach($dataArray as $key => $value) {
                    if ($key == 'store_id') {
                        for ($i = 0; $i < count($dataArray['store_id']['store']); $i++) {
                            $data[$key][$i] = $dataArray['store_id']['store'][$i];
                        }
                    }
                    else if ($key == 'content') {
                        for ($i = 0; $i < count($blockIds); $i++) {
                            $find = '<insert-block-' . $i . '-here>';
                            $replace = $blockIds[$i];
                            $value = str_replace($find,$replace,$value);
                        }
                        $data[$key] = $value;
                    }
                    else $data[$key] = $value;
                }
                $cmsPage->setPageId($pageId);
                $cmsPage->setData($data);
                $cmsPage->save();
            }
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    	$this->symlinkImages();    	
    	$blockIds = $this->importBlocks();
    	$this->importPage($blockIds);
    }
}